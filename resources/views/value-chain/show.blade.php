@extends('layouts.app')

@section('meta_title', 'Gráfico de la cadena de valor | ' . config('app.name'))

@section('section_title', 'Gráfico de la cadena de valor')

@section('styles')
    <style>
        .verticalTableHeader {
            writing-mode: vertical-rl;
            transform: rotate(180deg);
        }
        .table {
            vertical-align: middle;
        }
    </style>
@endsection

@section('content')
    <div class="card-box text-center">
        <div class="row">
            <div class="col-sm-1" style="padding-left: 20px; padding-right: 0px;">
                <table class="table table-bordered" style="margin-bottom: 0px">
                    <tr style="height: {{ ($sum_AP + 4)*37 }}px">
                        <th class="success" rowspan="{{ $sum_AP + 4 }}" style="vertical-align: middle"><p class="verticalTableHeader text-center" style="line-height: 3.6;height: 140px">Actividades de Apoyo</p></th>
                    </tr>
                </table>
                <table class="table table-bordered">
                    <tr style="height: {{ ($maxN+1)*57 }}px">
                        <th class="success" rowspan="{{$maxN+1}}" style="vertical-align: middle"><p class="verticalTableHeader text-center" style="line-height: 3.6;height: 140px">Actividades primarias</p></th>
                    </tr>
                </table>
            </div>
            <div class="col-sm-10" style="padding-left: 0px; padding-right: 0px;">
                <table class="table table-bordered">
                    <tr>
                        <th colspan="5" class="info text-center">Infraestructura</th>
                    </tr>
                    @foreach($infrastructures as $infrastructure)
                        <tr>
                            <td colspan="5">{{ $infrastructure->name }}</td>
                        </tr>
                    @endforeach
                    <tr class="info">
                        <th colspan="5" class="text-center">Recursos Humanos</th>
                    </tr>
                    @foreach($resources as $resource)
                        <tr>
                            <td colspan="5">{{ $resource->name }}</td>
                        </tr>
                    @endforeach
                    <tr class="info">
                        <th colspan="5" class="text-center">Desarrollo Tecnológico</th>
                    </tr>
                    @foreach($developments as $development)
                        <tr>
                            <td colspan="5">{{ $development->name }}</td>
                        </tr>
                    @endforeach
                    <tr class="info">
                        <th colspan="5" class="text-center">Abastecimiento</th>
                    </tr>
                    @foreach($caterings as $catering)
                        <tr>
                            <td colspan="5">{{ $catering->name }}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <th class="info text-center">Logística interna</th>
                        <th class="info text-center">Operaciones</th>
                        <th class="info text-center">Logística externa</th>
                        <th class="info text-center">Mercadotecnia y Ventas</th>
                        <th class="info text-center">Servicio Post Venta</th>
                    </tr>
                    @for ($i=0; $i<$maxN; ++$i)
                        <tr>
                            <td>{{ isset($internals[$i]) ? $internals[$i]->name : '-' }}</td>
                            <td>{{ isset($operations[$i]) ? $operations[$i]->name : '-' }}</td>
                            <td>{{ isset($externals[$i]) ? $externals[$i]->name : '-' }}</td>
                            <td>{{ isset($marketings[$i]) ? $marketings[$i]->name : '-' }}</td>
                            <td>{{ isset($services[$i]) ? $services[$i]->name : '-' }}</td>
                        </tr>
                    @endfor
                </table>
            </div>
            <div class="col-sm-1" style="padding-left: 0px; padding-right: 20px;">
                <table class="table table-bordered">
                    <tr style="height: {{ ($sum_AP + 4)*37 + ($maxN + 1)*57 }}px">
                        <th class="success" style="vertical-align: middle"><p class="verticalTableHeader text-center" style="line-height: 3.6;height: 280px">Margen</p></th>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')

@endsection