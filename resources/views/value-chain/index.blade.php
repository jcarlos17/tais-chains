@extends('layouts.app')

@section('meta_title', 'Datos de la cadena de valor | '.$company->name.' | '.config('app.name'))

@section('section_title', 'Datos de la cadena de valor')

@section('styles')
    <!-- Custom box css -->
    <link href="{{ asset('/plugins/custombox/dist/custombox.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <h3 class="text-primary font-600 m-t-0 m-b-15">Empresa: {{ $company->name }}</h3>
    <div class="row">
        <div class="col-sm-6">
            <div class="card-box">
                <h3 class="text-danger font-600 m-t-0 m-b-15">Actividades de apoyo</h3>
                <h4 class="header-title m-t-0 m-b-15">Infraestructura</h4>
                <ul>
                    @foreach($infrastructures as $infrastructure)
                        <li>
                            {{ $infrastructure->name }}
                        </li>
                    @endforeach
                </ul>
                <h4 class="header-title m-t-15 m-b-15">Recursos humanos</h4>
                <ul>
                    @foreach($resources as $resource)
                        <li>{{ $resource->name }}</li>
                    @endforeach
                </ul>
                <h4 class="header-title m-t-15 m-b-15">Desarrollo tecnológico</h4>
                <ul>
                    @foreach($developments as $development)
                        <li>{{ $development->name }}</li>
                    @endforeach
                </ul>
                <h4 class="header-title m-t-15 m-b-15">Abastecimiento</h4>
                <ul>
                    @foreach($caterings as $catering)
                        <li>{{ $catering->name }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card-box">
                <h3 class="text-danger font-600 m-t-0 m-b-15">Actividades principales</h3>
                <h4 class="header-title m-t-0 m-b-15">Logística interna</h4>
                <ul>
                    @foreach($internals as $internal)
                        <li>{{ $internal->name }}</li>
                    @endforeach
                </ul>
                <h4 class="header-title m-t-15 m-b-15">Operaciones</h4>
                <ul>
                    @foreach($operations as $operation)
                        <li>{{ $operation->name }}</li>
                    @endforeach
                </ul>
                <h4 class="header-title m-t-15 m-b-15">Servicio post-venta</h4>
                <ul>
                    @foreach($services as $service)
                        <li>{{ $service->name }}</li>
                    @endforeach
                </ul>
                <h4 class="header-title m-t-15 m-b-15">Logística externa</h4>
                <ul>
                    @foreach($externals as $external)
                        <li>{{ $external->name }}</li>
                    @endforeach
                </ul>
                <h4 class="header-title m-t-15 m-b-15">Mercadotecnia y ventas</h4>
                <ul>
                    @foreach($marketings as $marketing)
                        <li>{{ $marketing->name }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="hidden-print">
        <div class="pull-right">
            <a href="javascript:window.print()" class="btn btn-purple btn-rounded btn-bordred waves-effect waves-light"><i class="fa fa-print m-r-5"></i>Imprimir</a>
        </div>
    </div>
@endsection