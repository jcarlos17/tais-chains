@extends('layouts.form')

@section('meta_title', 'Iniciar sesión | ' . config('app.name'))

@section('content')
    <div class="wrapper-page" style="width: 90%">
        <div class="text-center">
            <p class="logo"><span><span>Temas Avanzados de Ingeniería de Sistemas II</span></span></p>
            <h3 class="text-white m-t-20 font-600">CADENA DE VALOR Y CADENA DE SUMINISTROS</h3>
        </div>
        <div class="row">
            <div class="col-sm-4 m-t-20">
                <img src="{{ asset('/images/gallery/foto_izquierda.jpg') }}" alt="" class="img-thumbnail img-responsive m-t-40">
            </div>
            <div class="col-sm-4">
                <div class="m-t-40 card-box">
                    <div class="text-center">
                        <h4 class="text-uppercase text-success font-600 m-b-0">Bienvenido al sistema de información web</h4>
                    </div>
                    <div class="panel-body">
                        <p class="font-600 text-justify">Aquí manejaremos principalmente la cadena de valor y la cadena de suministros
                            aplicandolo a diferentes empresas.<br>
                            Además manejaremos una gestión de usuarios.</p>
                    </div>
                    @if (Route::has('login'))
                        <div class="text-center m-t-20">
                            @auth
                                <a href="{{ url('/home') }}" class="btn btn-info btn-rounded btn-bordred w-lg waves-effect waves-light">INICIO</a>
                            @else
                                <a href="{{ route('login') }}" class="btn btn-primary btn-rounded btn-bordred w-lg waves-effect waves-light"><i class="fa fa-sign-in m-r-5"></i>Iniciar sesión</a>
                            @endauth
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-sm-4 m-t-20">
                <img src="{{ asset('/images/gallery/foto_derecha.png') }}" alt="" class="img-thumbnail img-responsive m-t-40">
            </div>
        </div>

    </div>
@endsection
