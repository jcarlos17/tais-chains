@extends('layouts.app')

@section('meta_title', 'Generalidades | ' . config('app.name'))

@section('section_title', 'Generalidades de la empresa')

@section('content')
    <div class="row">
        <div class="col-sm-8">
            <div class="card-box">
                <h5><span class="font-600"><i class="fa fa-building-o m-r-5"></i>Nombre empresarial: </span><span>{{ $company->name }}</span></h5>
                <hr>
                <h5><span class="font-600">RUC: </span><span>{{ $company->ruc }}</span></h5>
                <hr>
                <h5><span class="font-600"><i class="fa fa-phone m-r-5"></i>Teléfono: </span><span>{{ $company->phone }}</span></h5>
                <hr>
                <h5><span class="font-600"><i class="fa fa-envelope m-r-5"></i>Correo: </span><span>{{ $company->email }}</span></h5>
                <hr>
                <h5><span class="font-600"><i class="fa fa-map-marker m-r-5"></i>Ubicación: </span><span>{{ $company->location }}</span></h5>
                <hr>
                <h5><span class="font-600"><i class="fa fa-check-square-o m-r-5"></i>Tipo de contribuyente: </span><span>{{ $company->type_taxpayer }}</span></h5>
                <hr>
                <h5><span class="font-600"><i class="fa fa-check-square-o m-r-5"></i>Giro del negocio: </span><span>{{ $company->business_turn }}</span></h5>
                <hr>
                <h5><span class="font-600"><i class="fa fa-check-square-o m-r-5"></i>Domicilio fiscal: </span><span>{{ $company->tax_residence }}</span></h5>
            </div>
            @if(auth()->user()->is_admin)
                <div class="m-t-20">
                    <a href="{{ url('/generalities/'.$company->id.'/edit') }}" class="btn btn-success btn-rounded w-lg btn-bordred waves-effect waves-light m-r-5 m-b-10"><i class="fa fa-edit m-r-5"></i>Editar</a>
                </div>
            @endif
        </div>
        <div class="col-sm-4">
            <div class="panel panel-color panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-thumb-tack m-r-5"></i>Misión</h3>
                </div>
                <div class="panel-body">
                    <p>
                        {{ $company->mission }}
                    </p>
                </div>
            </div>
            <div class="panel panel-color panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-thumb-tack m-r-5"></i>Visión</h3>
                </div>
                <div class="panel-body">
                    <p>
                        {{ $company->vision }}
                    </p>
                </div>
            </div>
            <div class="panel panel-color panel-purple">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-thumb-tack m-r-5"></i>Propuesta de valor</h3>
                </div>
                <div class="panel-body">
                    <p>
                        {{ $company->value_proposal }}
                    </p>
                </div>
            </div>
            <div class="panel panel-color panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-thumb-tack m-r-5"></i>Factor diferenciador</h3>
                </div>
                <div class="panel-body">
                    <p>
                        {{ $company->factor }}
                    </p>
                </div>
            </div>
            <div class="panel panel-color panel-inverse">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-thumb-tack m-r-5"></i>Rol empresarial</h3>
                </div>
                <div class="panel-body">
                    <p>
                        {{ $company->business_role }}
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection
