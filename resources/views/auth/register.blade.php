@extends('layouts.form')

@section('meta_title', 'Registro | ' . config('app.name'))

@section('content')
    <div class="wrapper-page" style="margin: 4% auto;">
        <div class="m-t-30 card-box">
            <div class="text-center">
                <h4 class="text-uppercase font-bold m-b-0">Registro de usuarios</h4>
            </div>
            <div class="panel-body">
                <form class="form-horizontal m-t-20" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <div class="col-xs-12">
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Nombres" required autofocus>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                        <div class="col-xs-12">
                            <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" placeholder="Apellidos" required autofocus>
                            @if ($errors->has('last_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="col-xs-12">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="col-xs-12">
                            <input id="password" type="password" class="form-control" name="password" placeholder="Contraseña" required>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirmar contraseña" required>
                        </div>
                    </div>

                    {{--<div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">--}}
                        {{--<div class="col-xs-12">--}}
                            {{--<p class="text-muted font-13">Tipo de usuario</p>--}}
                            {{--<div class="radio radio-info radio-inline">--}}
                                {{--<input type="radio" id="option1" value="0" name="role" @if(old('role') == 0) checked @endif required>--}}
                                {{--<label for="option1"> Administrador </label>--}}
                            {{--</div>--}}
                            {{--<div class="radio radio-inline">--}}
                                {{--<input type="radio" id="option2" value="1" name="role" @if(old('role') == 1) checked @endif>--}}
                                {{--<label for="option2"> Usuario estándar </label>--}}
                            {{--</div>--}}
                            {{--@if ($errors->has('role'))--}}
                                {{--<span class="help-block">--}}
                                    {{--<strong>{{ $errors->first('role') }}</strong>--}}
                                {{--</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="form-group text-center m-t-40">
                        <div class="col-xs-12">
                            <button class="btn btn-custom btn-bordred btn-block waves-effect waves-light" type="submit">
                                Solicitar permiso
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-center">
                <a href="{{ url('/') }}" class="btn btn-danger btn-rounded btn-bordred w-lg waves-effect waves-light">Salir</a>
            </div>
        </div>
    </div>
@endsection
