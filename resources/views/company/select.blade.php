@extends('layouts.form')

@section('meta_title', 'Seleccione una empresa | ' . config('app.name'))


@section('content')
    <div class="wrapper-page" style="margin: 4% auto;">
        <div class="m-t-40 card-box">
            <div class="text-center">
                <h4 class="text-uppercase font-bold m-b-20">Seleccione una empresa</h4>
            </div>
            @if (session('notification'))
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('notification') }}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="panel-body">
                <form class="form-horizontal" method="POST" action="">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <select class="form-control select2" name="company_id" required>
                            <option value="">-Seleccione aquí-</option>
                            @foreach ($companies as $company)
                                <option value="{{ $company->id }}">{{ $company->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group text-center m-t-30 m-b-20">
                        <button class="btn btn-success btn-bordred btn-block waves-effect waves-light" type="submit">
                            Aceptar
                        </button>
                    </div>
                </form>
                @if(auth()->user()->is_admin)
                    <div class="col-sm-12 text-center m-t-20">
                        <p class="text-muted">¿Aún no esta registrada su empresa?</p>
                    </div>
                    <div class="text-center m-t-20">
                        <a href="{{ url('/company/register') }}" class="btn btn-primary btn-rounded w-lg btn-bordred waves-effect waves-light m-r-5 m-b-10">Registrar</a>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-center">
                <a href="{{ url('/logout') }}" class="btn btn-danger btn-rounded btn-bordred w-lg waves-effect waves-light"
                   onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                    <i class="fa fa-sign-out m-r-5"></i>Cerrar sesión
                </a>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>

@endsection
