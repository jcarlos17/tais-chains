@extends('layouts.app')

@section('meta_title', 'Inicio | ' . config('app.name'))

@section('section_title', 'Bienvenido')

@section('content')
    <div class="row">
        <div class="col-sm-6">
            <img src="{{ asset('/images/gallery/imagen.jpg') }}" alt="" class="img-thumbnail img-responsive">
        </div>
        <div class="col-sm-5 m-t-40">
            <div class="card-box">
                <div class="text-center">
                    <h4 class="text-uppercase text-success font-600 m-b-0">Bienvenido al sistema de información web</h4>
                </div>
                <div class="panel-body">
                    <p class="font-600 text-justify"></p>
                    <p class="font-600 text-justify">Con el menú vertical de la izquierda podremos manejar
                        diferentes gestiones:</p>
                    <ul class="font-600">
                        <li>Gestión de la cadena de valor.</li>
                        <li>Gestión de la cadena de suministros.</li>
                    </ul>
                    @if(auth()->user()->is_admin)
                    <p class="font-600 text-justify">Y el administrador podrá manejar:</p>
                    <ul class="font-600">
                        <li>Gestión de usuarios.</li>
                        <li>Gestión de empresas.</li>
                    </ul>
                    @endif
                    <p class="font-600 text-justify">Comencemos...</p>
                </div>
            </div>
        </div>
    </div>
@endsection