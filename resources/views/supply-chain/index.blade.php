@extends('layouts.app')

@section('meta_title', 'Datos de la cadena de suministros | ' .$company->name.' | '. config('app.name'))

@section('section_title', 'Datos de la cadena de suministros')

@section('styles')

@endsection

@section('content')
    <h3 class="text-success font-600 m-t-0 m-b-15">Empresa: {{ $company->name }}</h3>
    <div class="row">
        <div class="col-lg-6">
            <div class="card-box">
                <h3 class="text-primary m-t-0 m-b-15">Datos del proveedor:</h3>
                <h4 class="header-title m-t-0 m-b-15">Niveles de la cadena de suministro</h4>
                <ul>
                    @for ($i=1; $i<= $company->provider_levels_count; ++$i)
                        <li>Nivel {{ $i }}</li>
                    @endfor
                </ul>
                <h4 class="header-title m-t-15 m-b-15">Componentes de la cadena de suministro</h4>
                <table class="table table-bordered table-hover m-0">
                    <thead>
                    <tr class="active">
                        <th>Nombre</th>
                        <th class="text-center">Nivel</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($p_components as $p_component)
                        <tr>
                            <td>{{ $p_component->name }}</td>
                            <td class="text-center">{{ $p_component->level }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <h4 class="header-title m-t-15 m-b-15">Relaciones "Proveedor de Proveedor"</h4>
                <table class="table table-bordered table-hover m-0">
                    <thead>
                    <tr class="active">
                        <th>Relación Origen - Destino</th>
                        <th class="text-center">Origen</th>
                        <th class="text-center">Destino</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($p_relations as $p_relation)
                        <tr>
                            <td>{{ $p_relation->name_relation }}</td>
                            <td class="text-center">{{ $p_relation->origin->name }}</td>
                            <td class="text-center">{{ $p_relation->destiny->name }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card-box">
                <h3 class="text-primary m-t-0 m-b-15">Datos del cliente:</h3>
                <h4 class="header-title m-t-0 m-b-15">Niveles de la cadena de suministro</h4>
                <ul>
                    @for ($i=1; $i<= $company->customer_levels_count; ++$i)
                        <li>Nivel {{ $i }}</li>
                    @endfor
                </ul>
                <h4 class="header-title m-t-15 m-b-15">Componentes de la cadena de suministro</h4>
                <table class="table table-bordered table-hover m-0">
                    <thead>
                    <tr class="active">
                        <th>Nombre</th>
                        <th class="text-center">Nivel</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($c_components as $c_component)
                        <tr>
                            <td>{{ $c_component->name }}</td>
                            <td class="text-center">{{ $c_component->level }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <h4 class="header-title m-t-15 m-b-15">Relaciones "Cliente de Cliente"</h4>
                <table class="table table-bordered table-hover m-0">
                    <thead>
                    <tr class="active">
                        <th>Relación Origen - Destino</th>
                        <th class="text-center">Origen</th>
                        <th class="text-center">Destino</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($c_relations as $c_relation)
                        <tr>
                            <td>{{ $c_relation->name_relation }}</td>
                            <td class="text-center">{{ $c_relation->origin->name }}</td>
                            <td class="text-center">{{ $c_relation->destiny->name }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<div class="hidden-print">
    <div class="pull-right">
        <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-print m-r-5"></i>Imprimir</a>
    </div>
</div>
@endsection

@section('script')

@endsection