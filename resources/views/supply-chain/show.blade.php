@extends('layouts.app')

@section('meta_title', 'Gráfico | ' . config('app.name'))

@section('section_title', 'Gráfico de la cadena de suministros')

@section('styles')
    <link rel="stylesheet" href="https://jsplumbtoolkit.com/css/jsplumbtoolkit-defaults.css">
    <link rel="stylesheet" href="https://jsplumbtoolkit.com/css/jsplumbtoolkit-demo.css">

    <link href="https://jsplumbtoolkit.com/community/demo/statemachine/app.css" rel="stylesheet">
@endsection

@section('content')
<!-- demo -->
<div class="jtk-demo-canvas canvas-wide statemachine-demo jtk-surface jtk-surface-nopan" id="canvas">
    @foreach($providers as $provider)
        <div class="w" id="provider{{ $provider->id }}" style="left: {{ $provider->x }}px; top: {{ $provider->y }}px;">
            {{ $provider->name }}
            <div action="{{ $provider->name }}"></div>
        </div>
    @endforeach
    @foreach($customers as $customer)
        <div class="w" id="customer{{ $customer->id }}" style="left: {{ $customer->x }}px; top: {{ $customer->y }}px;">
            {{ $customer->name }}
            <div action="{{ $customer->name }}"></div>
        </div>
    @endforeach
    <div class="w" id="company{{ $company->id }}" style="left: {{ $company->x }}px; top: {{ $company->y }}px;">
        {{ $company->name }}
        <div action="{{ $company->name }}"></div>
    </div>
</div>
<!-- /demo -->
@endsection

@section('script')
    <script src="{{ asset('/js/graphic/jsplumb.min.js') }}"></script>
    <script>
        jsPlumb.ready(function () {

            // setup some defaults for jsPlumb.
            var instance = jsPlumb.getInstance({
                Endpoint: ["Dot", {radius: 2}],
                Connector:"StateMachine",
                HoverPaintStyle: {stroke: "#1e8151", strokeWidth: 2 },
                ConnectionOverlays: [
                    [ "Arrow", {
                        location: 1,
                        id: "arrow",
                        length: 14,
                        foldback: 0.8
                    } ]
                ],
                Container: "canvas"
            });

            instance.registerConnectionType("basic", { anchor:"Continuous", connector:"StateMachine" });

            window.jsp = instance;

            var canvas = document.getElementById("canvas");
            var windows = jsPlumb.getSelector(".statemachine-demo .w");

            // jsPlumb.on(canvas, "dblclick", function(e) {
            //     newNode(e.offsetX, e.offsetY);
            // });

            // initialise element as connection targets and source.
            var initNode = function(el) {

                // initialise draggable elements.
                instance.draggable(el, {
                    stop: function(data) {
                        var params = {
                            id: data.drag.el.id,
                            x: data.finalPos[0],
                            y: data.finalPos[1]
                        };
                        $.getJSON('/supply-chain/graphic/position', params, function (data) {
                            if (data.success) {
                                console.log('updated position via ajax');
                            } else {
                                console.log('error updating position via ajax');
                            }
                        });
                        /*console.log(data.drag.el.id);
                        console.log(data.finalPos[0]);
                        console.log(data.finalPos[1]);*/
                    }
                });

                instance.makeSource(el, {
                    filter: ".ep",
                    anchor: "Continuous",
                    connectorStyle: { stroke: "#5c96bc", strokeWidth: 2, outlineStroke: "transparent", outlineWidth: 4 },
                    connectionType:"basic",
                    extract:{
                        "action":"the-action"
                    },
                    maxConnections: 2,
                    onMaxConnections: function (info, e) {
                        alert("Maximum connections (" + info.maxConnections + ") reached");
                    }
                });

                instance.makeTarget(el, {
                    dropOptions: { hoverClass: "dragHover" },
                    anchor: "Continuous",
                    allowLoopback: true
                });

                // this is not part of the core demo functionality; it is a means for the Toolkit edition's wrapped
                // version of this demo to find out about new nodes being added.
                //
                instance.fire("jsPlumbDemoNodeAdded", el);
            };

            // suspend drawing and initialise.
            instance.batch(function () {
                for (var i = 0; i < windows.length; i++) {
                    initNode(windows[i], true);
                }
                // and finally, make a few connections
                @foreach($customerRels as $customerRel)
                    instance.connect({ source: "customer{{ $customerRel->origin_customer_id }}", target: "customer{{ $customerRel->destiny_customer_id }}", type:"basic" });
                @endforeach
                @foreach($providerRels as $providerRel)
                    instance.connect({ source: "provider{{ $providerRel->origin_provider_id }}", target: "provider{{ $providerRel->destiny_provider_id }}", type:"basic" });
                @endforeach
                @foreach($Lv1_providers as $Lv1_provider)
                    instance.connect({ source: "provider{{ $Lv1_provider->id }}", target: "company{{ $company->id }}", type:"basic" });
                @endforeach
                @foreach($Lv1_customers as $Lv1_customer)
                    instance.connect({ source: "company{{ $company->id }}", target: "customer{{ $Lv1_customer->id }}", type:"basic" });
                @endforeach
            });

            jsPlumb.fire("jsPlumbDemoLoaded", instance);

        });
    </script>
@endsection