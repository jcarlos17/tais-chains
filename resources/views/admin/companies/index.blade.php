@extends('layouts.app')

@section('meta_title', 'Lista de empresas | ' . config('app.name'))

@section('section_title', 'Lista de empresas')

@section('styles')

@endsection

@section('content')
    <div class="card-box">
        <h4 class="header-title m-t-0 m-b-30">Lista de empresas</h4>
        <table class="table table-bordered table-hover m-0">
            <thead>
            <tr class="active">
                <th>Nombre empresa</th>
                <th class="text-center">RUC</th>
                <th class="text-center col-sm-1">Opción</th>
            </tr>
            </thead>
            <tbody>
            @foreach($companies as $company)
                <tr>
                    <td>{{ $company->name }}</td>
                    <td class="text-center col-sm-2">{{ $company->ruc }}</td>
                    <td class="text-center">

                        <a href="{{ url('/companies/'.$company->id.'/delete') }}" class="btn btn-sm btn-danger" title="Eliminar" onclick="return confirm('¿Estas seguro que quieres eliminar?')">
                            <i class="fa fa-trash o"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

@section('scripts')

@endsection
