@extends('layouts.app')

@section('meta_title', 'Editar empresa | ' . config('app.name'))

@section('section_title', 'Editar empresa')

@section('styles')

@endsection

@section('content')
    <div class="card-box">
        <h4 class="text-uppercase font-bold m-b-20">Editar</h4>
        <form class="form-group" role="form" action="{{ url('/generalities/'.$company->id.'/edit') }}" method="POST">
            {{ csrf_field() }}

            <div class="row">
                <div class="col-md-5">
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="control-label">Nombre empresarial<span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="name" id="name" value="{{ old('name', $company->name) }}">
                        @if ($errors->has('name'))
                            <span class="help-block font-13">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group{{ $errors->has('ruc') ? ' has-error' : '' }}">
                        <label for="moreoptions" class="control-label">RUC<span class="text-danger">*</span></label>
                        <input type="text" min="0" class="form-control" name="ruc" value="{{ old('ruc', $company->ruc) }}" maxlength="11" id="moreoptions" onkeypress="return justNumbers(event);">
                        @if ($errors->has('ruc'))
                            <span class="help-block font-13">
                                <strong>{{ $errors->first('ruc') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group{{ $errors->has('type_taxpayer') ? ' has-error' : '' }}">
                        <label for="type_taxpayer" class="control-label">Tipo de contribuyente<span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="type_taxpayer" id="type_taxpayer" value="{{ old('type_taxpayer', $company->type_taxpayer) }}">
                        @if ($errors->has('type_taxpayer'))
                            <span class="help-block font-13">
                                <strong>{{ $errors->first('type_taxpayer') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group{{ $errors->has('business_turn') ? ' has-error' : '' }}">
                        <label for="business_turn" class="control-label">Giro del negocio<span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="business_turn" id="business_turn" value="{{ old('business_turn', $company->business_turn) }}">
                        @if ($errors->has('business_turn'))
                            <span class="help-block font-13">
                                <strong>{{ $errors->first('business_turn') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('tax_residence') ? ' has-error' : '' }}">
                        <label for="tax_residence" class="control-label">Domicilio fiscal<span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="tax_residence" id="tax_residence" value="{{ old('tax_residence', $company->tax_residence) }}">
                        @if ($errors->has('tax_residence'))
                            <span class="help-block font-13">
                        <strong>{{ $errors->first('tax_residence') }}</strong>
                    </span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                        <label for="phone" class="control-label">Teléfono fijo o móvil</label>
                        <input type="text" min="0" class="form-control" name="phone" value="{{ old('phone', $company->phone) }}" maxlength="9" id="moreoptions" onkeypress="return justNumbers(event);">
                        @if ($errors->has('phone'))
                            <span class="help-block font-13">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="control-label">Correo</label>
                        <input type="email" class="form-control" name="email" id="email" value="{{ old('email', $company->email) }}">
                        @if ($errors->has('email'))
                            <span class="help-block font-13">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                        <label for="location" class="control-label">Ubicación</label>
                        <input type="text" class="form-control" name="location" id="location" value="{{ old('location', $company->location) }}">
                        @if ($errors->has('location'))
                            <span class="help-block font-13">
                        <strong>{{ $errors->first('location') }}</strong>
                    </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('mission') ? ' has-error' : '' }}">
                        <label for="mission" class="control-label">Misión</label>
                        <textarea class="form-control" name="mission" id="mission" rows="4">{{ old('mission', $company->mission) }}</textarea>
                        @if ($errors->has('mission'))
                            <span class="help-block font-13">
                        <strong>{{ $errors->first('mission') }}</strong>
                    </span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('vision') ? ' has-error' : '' }}">
                        <label for="vision" class="control-label">Visión</label>
                        <textarea class="form-control" name="vision" id="vision" rows="4">{{ old('vision', $company->vision) }}</textarea>
                        @if ($errors->has('vision'))
                            <span class="help-block font-13">
                        <strong>{{ $errors->first('vision') }}</strong>
                    </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('value_proposal') ? ' has-error' : '' }}">
                        <label for="value_proposal" class="control-label">Propuesta de valor</label>
                        <textarea class="form-control" name="value_proposal" id="value_proposal" rows="4">{{ old('value_proposal', $company->value_proposal) }}</textarea>
                        @if ($errors->has('value_proposal'))
                            <span class="help-block font-13">
                        <strong>{{ $errors->first('value_proposal') }}</strong>
                    </span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group{{ $errors->has('factor') ? ' has-error' : '' }}">
                        <label for="factor" class="control-label">Factor diferenciador</label>
                        <textarea class="form-control" name="factor" id="factor" rows="4">{{ old('factor', $company->factor) }}</textarea>
                        @if ($errors->has('factor'))
                            <span class="help-block font-13">
                        <strong>{{ $errors->first('factor') }}</strong>
                    </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="form-group{{ $errors->has('business_role') ? ' has-error' : '' }}">
                <label for="business_role" class="control-label">Rol empresarial</label>
                <textarea class="form-control" name="business_role" id="business_role" rows="3">{{ old('business_role', $company->business_role) }}</textarea>
                @if ($errors->has('business_role'))
                    <span class="help-block font-13">
                        <strong>{{ $errors->first('business_role') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group m-t-20">
                <a href="{{ url('/generalities') }}" class="btn btn-default btn-rounded w-lg btn-bordred waves-effect waves-light m-r-5 m-b-10">Volver</a>
                <button type="submit" class="btn btn-primary btn-rounded w-lg btn-bordred waves-effect waves-light m-r-5 m-b-10">Guardar cambios</button>
            </div>

        </form>
        <span class="font-13 text-danger">* Campos obligatorios</span>
    </div>
@endsection

@section('script')
    <script src="{{ asset('/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>
    <script>
        $('input#moreoptions').maxlength({
            alwaysShow: true,
            warningClass: "label label-success",
            limitReachedClass: "label label-danger"
        });
    </script>
    <script>
        function justNumbers(e)
        {
            var keynum = window.event ? window.event.keyCode : e.which;
            if ((keynum === 8))
                return true;
            return /\d/.test(String.fromCharCode(keynum));
        }
    </script>
@endsection