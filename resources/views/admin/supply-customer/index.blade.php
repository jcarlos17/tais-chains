@extends('layouts.app')

@section('meta_title', 'Datos de la cadena de suministro: Cliente | '.$company->name.' | '  . config('app.name'))

@section('section_title', 'Datos de la cadena de suministro: Cliente')

@section('styles')
    <!-- Custom box css -->
    <link href="{{ asset('/plugins/custombox/dist/custombox.min.css') }}" rel="stylesheet">

    <link href="{{ asset('/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-5">
            <div class="card-box">
                <div class="pull-right hidden-print">
                    <a href="{{ url('/supply-chain/customer/'.$company->id.'/addLevel') }}" class="btn btn-sm btn-primary waves-effect waves-light">
                        Agregar nivel
                    </a>
                </div>
                <h4 class="header-title m-t-0 m-b-30">Niveles de la cadena de suministro</h4>
                <table class="table table-bordered table-hover m-0">
                    <thead>
                    <tr class="active">
                        <th>Niveles del cliente</th>
                        <th class="hidden-print text-center col-sm-3">Opción</th>
                    </tr>
                    </thead>
                    <tbody>
                    @for ($i=1; $i<= $company->customer_levels_count; ++$i)
                    <tr>
                        <td>Nivel {{ $i }}</td>
                        @if($i == $company->customer_levels_count)
                            <td class="hidden-print text-center">
                                <a href="{{ url('/supply-chain/customer/'.$company->id.'/removeLevel') }}" class="btn btn-sm btn-danger" title="Eliminar" onclick="return confirm('¿Estas seguro que quieres eliminar?')">
                                    <i class="fa fa-trash o"></i>
                                </a>
                            </td>
                        @else
                            <td class="hidden-print text-center">
                                <button class="btn btn-sm btn-danger" title="No se puede eliminar sin eliminar el nivel más alto" disabled>
                                    <i class="fa fa-trash o"></i>
                                </button>
                            </td>
                        @endif
                    </tr>
                    @endfor
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-lg-7">
            <div class="card-box">
                <div class="pull-right hidden-print">
                    <button class="btn btn-sm btn-primary waves-effect waves-light" data-toggle="modal" data-target="#new-component">
                        Nuevo
                    </button>
                </div>
                <h4 class="header-title m-t-0 m-b-30">Componentes de la cadena de suministro</h4>
                <table class="table table-bordered table-hover m-0">
                    <thead>
                    <tr class="active">
                        <th>Nombre</th>
                        <th class="text-center">Nivel</th>
                        <th class="hidden-print text-center col-sm-3">Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($components as $component)
                        <tr>
                            <td>{{ $component->name }}</td>
                            <td class="text-center">{{ $component->level }}</td>
                            <td class="hidden-print text-center">
                                <button type="button" class="btn btn-sm btn-success" title="Editar"
                                        data-component="{{ $component->id }}" @if($component->editable) data-editable="1" @else data-editable="0" @endif>
                                    <i class="fa fa-edit"></i>
                                </button>
                                <a href="{{ url('/supply-chain/customer/'.$component->id.'/deleteComponent') }}" class="btn btn-sm btn-danger" title="Eliminar" onclick="return confirm('¿Estas seguro que quieres eliminar?')">
                                    <i class="fa fa-trash o"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-10">
            <div class="card-box">
                <div class="pull-right hidden-print">
                    <button class="btn btn-sm btn-primary waves-effect waves-light" data-toggle="modal" data-target="#new-relation">
                        Nuevo
                    </button>
                </div>
                <h4 class="header-title m-t-0 m-b-30">Relaciones "Cliente de Cliente"</h4>
                <table class="table table-bordered table-hover m-0">
                    <thead>
                    <tr class="active">
                        <th>Relación Origen - Destino</th>
                        <th class="text-center">Origen</th>
                        <th class="text-center">Destino</th>
                        <th class="hidden-print text-center col-sm-2">Opción</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($relations as $relation)
                        <tr>
                            <td>{{ $relation->name_relation }}</td>
                            <td class="text-center">{{ $relation->origin->name }}</td>
                            <td class="text-center">{{ $relation->destiny->name }}</td>
                            <td class="hidden-print text-center">
                                <a href="{{ url('/supply-chain/customer/'.$relation->id.'/deleteRelation') }}" class="btn btn-sm btn-danger" title="Eliminar" onclick="return confirm('¿Estas seguro que quieres eliminar?')">
                                    <i class="fa fa-trash o"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <div class="hidden-print">
        <div class="pull-right">
            <a href="javascript:window.print()" class="btn btn-purple btn-rounded btn-bordred waves-effect waves-light"><i class="fa fa-print m-r-5"></i>Imprimir</a>
        </div>
    </div>

    <div id="new-component" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Registrar nuevo componente</h4>
                </div>
                <form class="form-group" role="form" action="{{ url('/supply-chain/customer/newComponent') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label for="name" class="control-label">Nombre de la actividad</label>
                                    <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}" required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label for="level">Seleccione nivel</label>
                                <select id="level" name="level" class="form-control" required>
                                    @for ($i=1; $i<= $company->customer_levels_count; ++$i)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Registrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{--modal new relation--}}
    <div id="new-relation" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Registrar nueva relación</h4>
                </div>
                <form class="form-group" role="form" action="{{ url('/supply-chain/customer/newRelation') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <select name="origin_id" class="form-control"  id="select-origin">
                                    <option value="">Seleccione origen</option>
                                    @foreach ($components as $component)
                                        <option value="{{ $component->id }}">{{ $component->name }} ({{ $component->level }})</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <select name="destiny_id" class="form-control"  id="select-destiny">
                                    <option value="">Seleccione destino</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Registrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{--modal edit component--}}
    <div class="modal fade" tabindex="-1" role="dialog" id="modalComponent">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Editar componente</h4>
                </div>
                <form action="{{ url('/supply-chain/customer/changeComponent') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <input type="hidden" name="component_id" id="component_id" value="">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label for="component_name" class="control-label">Nombre de la actividad</label>
                                    <input type="text" class="form-control" name="name" id="component_name" value="{{ old('name') }}" required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label for="component_level">Seleccione nivel</label>
                                <select id="component_level" name="level" class="form-control" disabled>
                                    @for ($i=1; $i<= $company->customer_levels_count; ++$i)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Guardar cambios</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <!-- Modal-Effect -->
    <script src="{{ asset('/plugins/custombox/dist/custombox.min.js') }}"></script>
    <script src="{{ asset('/plugins/custombox/dist/legacy.min.js"><') }}"></script>

    <script src="{{ asset('/js/admin/supply/edit.js') }}"></script>

    <script src="{{ asset('/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js') }}" type="text/javascript"></script>
    <script>
        $("input[name='demo3']").TouchSpin({
            buttondown_class: "btn btn-primary",
            buttonup_class: "btn btn-primary"
        });
    </script>
@endsection