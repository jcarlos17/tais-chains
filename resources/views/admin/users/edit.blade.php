@extends('layouts.app')

@section('meta_title', 'Editar usuario | ' . config('app.name'))

@section('section_title', 'Editar usuario')

@section('styles')

@endsection

@section('content')
    <div class="row">
        <div class="col-sm-6">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30">Editar usuario: {{ $user->name }}</h4>
                <form class="form-group" method="POST" action="">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name">Nombre</label>
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name', $user->name) }}" placeholder="Nombres" required autofocus>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                <label for="last_name">Apellidos</label>
                                <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name', $user->last_name) }}" placeholder="Apellidos" required autofocus>
                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email">Correo:</label>
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email', $user->email) }}" placeholder="Email" required>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password">Contraseña <em>(Ingresar sólo si desea modificar)</em></label>
                        <input id="password" type="password" class="form-control" name="password" placeholder="Contraseña">
                        @if ($errors->has('password'))
                            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
                        @endif
                    </div>

                    <div class="form-group m-t-30">
                        <a href="{{ url('/users') }}" class="btn btn-default btn-rounded w-lg btn-bordred waves-effect waves-light m-r-5 m-b-10">Volver</a>
                        <button class="btn btn-primary btn-rounded w-lg btn-bordred waves-effect waves-light m-r-5 m-b-10" type="submit">
                            Guardar cambios
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
