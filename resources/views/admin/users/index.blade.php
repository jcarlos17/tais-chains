@extends('layouts.app')

@section('meta_title', 'Lista de usuarios | ' . config('app.name'))

@section('section_title', 'Lista de usuarios')

@section('styles')

@endsection

@section('content')
    <div class="card-box">
        <h4 class="header-title m-t-0 m-b-30">Lista de usuarios</h4>
        <table class="table table-bordered table-hover m-0">
            <thead>
            <tr class="active">
                <th>Nombres</th>
                <th>Apellidos</th>
                <th>Correo</th>
                <th>Tipo</th>
                <th class="text-center col-sm-1">Permiso</th>
                <th class="text-center col-sm-2">Opciones</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->last_name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->role_name }}</td>
                    <td class="text-center">
                        @if($user->active)
                            <input type="checkbox" checked onchange="window.location.href='/users/{{ $user->id }}/deactivate'" data-plugin="switchery" data-color="#00b19d" data-size="small"/>
                        @else
                            <input type="checkbox" onchange="window.location.href='/users/{{ $user->id }}/activate'" data-plugin="switchery" data-color="#00b19d" data-size="small"/>
                        @endif
                    </td>
                    <td class="text-center">
                        <a href="{{ url('/users/'.$user->id.'/edit') }}" class="btn btn-sm btn-success" title="Editar">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a href="{{ url('/users/'.$user->id.'/delete') }}" class="btn btn-sm btn-danger" title="Eliminar" onclick="return confirm('¿Estas seguro que quieres eliminar?')">
                            <i class="fa fa-trash o"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

@section('scripts')

@endsection
