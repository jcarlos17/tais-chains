@extends('layouts.app')

@section('meta_title', 'Gestión | '.$company->name.' | ' . config('app.name'))

@section('section_title', 'Gestión de la cadena de valor')

@section('styles')
    <!-- Custom box css -->
    <link href="{{ asset('/plugins/custombox/dist/custombox.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <h3 class="text-danger font-600 m-b-10">Actividades de apoyo</h3>
    <div class="row">
        <div class="col-lg-6">
            <div class="card-box">
                <div class="pull-right hidden-print">
                    <button class="btn btn-sm btn-primary waves-effect waves-light" data-toggle="modal" data-target="#new-infrastructure">
                        Nuevo
                    </button>
                </div>
                <h4 class="header-title m-t-0 m-b-30">Infraestructura</h4>
                <table class="table table table-hover m-0">
                    <thead>
                    <tr class="active">
                        <th>Nombre</th>
                        <th class="hidden-print col-sm-4">Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($infrastructures as $infrastructure)
                        <tr>
                            <td>{{ $infrastructure->name }}</td>
                            <td class="hidden-print">
                                <button type="button" class="btn btn-sm btn-success" title="Editar" data-infrastructure="{{ $infrastructure->id }}">
                                    <i class="fa fa-edit"></i>
                                </button>
                                <a href="{{ url('/activity/'.$infrastructure->id.'/delete') }}" class="btn btn-sm btn-danger" title="Eliminar" onclick="return confirm('¿Estas seguro que quieres eliminar?')">
                                    <i class="fa fa-trash o"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="card-box">
                <div class="pull-right hidden-print">
                    <button class="btn btn-sm btn-primary waves-effect waves-light" data-toggle="modal" data-target="#new-rr-hh">
                        Nuevo
                    </button>
                </div>
                <h4 class="header-title m-t-0 m-b-30">Recursos humanos</h4>
                <table class="table table table-hover m-0">
                    <thead>
                    <tr class="active">
                        <th>Nombre</th>
                        <th class="hidden-print col-sm-4">Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($resources as $resource)
                        <tr>
                            <td>{{ $resource->name }}</td>
                            <td class="hidden-print">
                                <button type="button" class="btn btn-sm btn-success" title="Editar" data-infrastructure="{{ $resource->id }}">
                                    <i class="fa fa-edit"></i>
                                </button>
                                <a href="{{ url('/activity/'.$resource->id.'/delete') }}" class="btn btn-sm btn-danger" title="Eliminar" onclick="return confirm('¿Estas seguro que quieres eliminar?')">
                                    <i class="fa fa-trash o"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card-box">
                <div class="pull-right hidden-print">
                    <button class="btn btn-sm btn-primary waves-effect waves-light" data-toggle="modal" data-target="#new-development">
                        Nuevo
                    </button>
                </div>
                <h4 class="header-title m-t-0 m-b-30">Desarrollo tecnológico</h4>
                <table class="table table table-hover m-0">
                    <thead>
                    <tr class="active">
                        <th>Nombre</th>
                        <th class="hidden-print col-sm-4">Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($developments as $development)
                        <tr>
                            <td>{{ $development->name }}</td>
                            <td class="hidden-print">
                                <button type="button" class="btn btn-sm btn-success" title="Editar" data-infrastructure="{{ $development->id }}">
                                    <i class="fa fa-edit"></i>
                                </button>
                                <a href="{{ url('/activity/'.$development->id.'/delete') }}" class="btn btn-sm btn-danger" title="Eliminar" onclick="return confirm('¿Estas seguro que quieres eliminar?')">
                                    <i class="fa fa-trash o"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="card-box">
                <div class="pull-right hidden-print">
                    <button class="btn btn-sm btn-primary waves-effect waves-light" data-toggle="modal" data-target="#new-catering">
                        Nuevo
                    </button>
                </div>
                <h4 class="header-title m-t-0 m-b-30">Abastecimiento</h4>
                <table class="table table table-hover m-0">
                    <thead>
                    <tr class="active">
                        <th>Nombre</th>
                        <th class="hidden-print col-sm-4">Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($caterings as $catering)
                        <tr>
                            <td>{{ $catering->name }}</td>
                            <td class="hidden-print">
                                <button type="button" class="btn btn-sm btn-success" title="Editar" data-infrastructure="{{ $catering->id }}">
                                    <i class="fa fa-edit"></i>
                                </button>
                                <a href="{{ url('/activity/'.$catering->id.'/delete') }}" class="btn btn-sm btn-danger" title="Eliminar" onclick="return confirm('¿Estas seguro que quieres eliminar?')">
                                    <i class="fa fa-trash o"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <h3 class="text-danger font-600 m-b-10">Actividades principales</h3>
    <div class="row">
        <div class="col-lg-6">
            <div class="card-box">
                <div class="pull-right hidden-print">
                    <button class="btn btn-sm btn-primary waves-effect waves-light" data-toggle="modal" data-target="#new-internal">
                        Nuevo
                    </button>
                </div>
                <h4 class="header-title m-t-0 m-b-30">Logística interna</h4>
                <table class="table table table-hover m-0">
                    <thead>
                    <tr class="active">
                        <th>Nombre</th>
                        <th class="hidden-print col-sm-4">Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($internals as $internal)
                        <tr>
                            <td>{{ $internal->name }}</td>
                            <td class="hidden-print">
                                <button type="button" class="btn btn-sm btn-success" title="Editar" data-infrastructure="{{ $internal->id }}">
                                    <i class="fa fa-edit"></i>
                                </button>
                                <a href="{{ url('/activity/'.$internal->id.'/delete') }}" class="btn btn-sm btn-danger" title="Eliminar" onclick="return confirm('¿Estas seguro que quieres eliminar?')">
                                    <i class="fa fa-trash o"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="card-box">
                <div class="pull-right hidden-print">
                    <button class="btn btn-sm btn-primary waves-effect waves-light" data-toggle="modal" data-target="#new-operation">
                        Nuevo
                    </button>
                </div>
                <h4 class="header-title m-t-0 m-b-30">Operaciones</h4>
                <table class="table table table-hover m-0">
                    <thead>
                    <tr class="active">
                        <th>Nombre</th>
                        <th class="hidden-print col-sm-4">Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($operations as $operation)
                        <tr>
                            <td>{{ $operation->name }}</td>
                            <td class="hidden-print">
                                <button type="button" class="btn btn-sm btn-success" title="Editar" data-infrastructure="{{ $operation->id }}">
                                    <i class="fa fa-edit"></i>
                                </button>
                                <a href="{{ url('/activity/'.$operation->id.'/delete') }}" class="btn btn-sm btn-danger" title="Eliminar" onclick="return confirm('¿Estas seguro que quieres eliminar?')">
                                    <i class="fa fa-trash o"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="card-box">
                <div class="pull-right hidden-print">
                    <button class="btn btn-sm btn-primary waves-effect waves-light" data-toggle="modal" data-target="#new-service">
                        Nuevo
                    </button>
                </div>
                <h4 class="header-title m-t-0 m-b-30">Servicio post-venta</h4>
                <table class="table table table-hover m-0">
                    <thead>
                    <tr class="active">
                        <th>Nombre</th>
                        <th class="hidden-print col-sm-4">Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($services as $service)
                        <tr>
                            <td>{{ $service->name }}</td>
                            <td class="hidden-print">
                                <button type="button" class="btn btn-sm btn-success" title="Editar" data-infrastructure="{{ $service->id }}">
                                    <i class="fa fa-edit"></i>
                                </button>
                                <a href="{{ url('/activity/'.$service->id.'/delete') }}" class="btn btn-sm btn-danger" title="Eliminar" onclick="return confirm('¿Estas seguro que quieres eliminar?')">
                                    <i class="fa fa-trash o"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card-box">
                <div class="pull-right hidden-print">
                    <button class="btn btn-sm btn-primary waves-effect waves-light" data-toggle="modal" data-target="#new-external">
                        Nuevo
                    </button>
                </div>
                <h4 class="header-title m-t-0 m-b-30">Logística externa</h4>
                <table class="table table table-hover m-0">
                    <thead>
                    <tr class="active">
                        <th>Nombre</th>
                        <th class="hidden-print col-sm-4">Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($externals as $external)
                        <tr>
                            <td>{{ $external->name }}</td>
                            <td class="hidden-print">
                                <button type="button" class="btn btn-sm btn-success" title="Editar" data-infrastructure="{{ $external->id }}">
                                    <i class="fa fa-edit"></i>
                                </button>
                                <a href="{{ url('/activity/'.$external->id.'/delete') }}" class="btn btn-sm btn-danger" title="Eliminar" onclick="return confirm('¿Estas seguro que quieres eliminar?')">
                                    <i class="fa fa-trash o"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="card-box">
                <div class="pull-right hidden-print">
                    <button class="btn btn-sm btn-primary waves-effect waves-light" data-toggle="modal" data-target="#new-marketing">
                        Nuevo
                    </button>
                </div>
                <h4 class="header-title m-t-0 m-b-30">Mercadotecnia y ventas</h4>
                <table class="table table table-hover m-0">
                    <thead>
                    <tr class="active">
                        <th>Nombre</th>
                        <th class="hidden-print col-sm-4">Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($marketings as $marketing)
                        <tr>
                            <td>{{ $marketing->name }}</td>
                            <td class="hidden-print">
                                <button type="button" class="btn btn-sm btn-success" title="Editar" data-infrastructure="{{ $marketing->id }}">
                                    <i class="fa fa-edit"></i>
                                </button>
                                <a href="{{ url('/activity/'.$marketing->id.'/delete') }}" class="btn btn-sm btn-danger" title="Eliminar" onclick="return confirm('¿Estas seguro que quieres eliminar?')">
                                    <i class="fa fa-trash o"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="hidden-print">
        <div class="pull-right">
            <a href="javascript:window.print()" class="btn btn-purple btn-rounded btn-bordred waves-effect waves-light"><i class="fa fa-print m-r-5"></i>Imprimir</a>
        </div>
    </div>
    {{--modal new infrastructure--}}
    <div id="new-infrastructure" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Registro para: Infraestructura</h4>
                </div>
                <form class="form-group" role="form" action="{{ url('/activity/create') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <p class="text-muted m-b-15 font-13">
                            Se esta realizando el registro de una nueva actividad en el campo: <strong>Infraestructura.</strong><br>
                            De lo contrario escoja otro campo...
                        </p>
                        <div class="form-group">
                            <label for="name" class="control-label">Nombre de la actividad</label>
                            <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}">
                            <input type="hidden" name="category" value="Infraestructura">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Registrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--modal new RR.HH.--}}
    <div id="new-rr-hh" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Registro para: Recursos humanos</h4>
                </div>
                <form class="form-group" role="form" action="{{ url('/activity/create') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <p class="text-muted m-b-15 font-13">
                            Se esta realizando el registro de una nueva actividad en el campo: <strong>Recursos humanos.</strong><br>
                            De lo contrario escoja otro campo...
                        </p>
                        <div class="form-group">
                            <label for="name" class="control-label">Nombre de la actividad</label>
                            <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}">
                            <input type="hidden" name="category" value="Recursos humanos">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Registrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--modal new development--}}
    <div id="new-development" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Registro para: Desarrollo tecnológico</h4>
                </div>
                <form class="form-group" role="form" action="{{ url('/activity/create') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <p class="text-muted m-b-15 font-13">
                            Se esta realizando el registro de una nueva actividad en el campo: <strong>Desarrollo tecnológico.</strong><br>
                            De lo contrario escoja otro campo...
                        </p>
                        <div class="form-group">
                            <label for="name" class="control-label">Nombre de la actividad</label>
                            <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}">
                            <input type="hidden" name="category" value="Desarrollo tecnológico">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Registrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--modal new catering--}}
    <div id="new-catering" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Registro para: Abastecimiento</h4>
                </div>
                <form class="form-group" role="form" action="{{ url('/activity/create') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <p class="text-muted m-b-15 font-13">
                            Se esta realizando el registro de una nueva actividad en el campo: <strong>Abastecimiento.</strong><br>
                            De lo contrario escoja otro campo...
                        </p>
                        <div class="form-group">
                            <label for="name" class="control-label">Nombre de la actividad</label>
                            <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}">
                            <input type="hidden" name="category" value="Abastecimiento">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Registrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{--modal edit infrastructure--}}
    <div class="modal fade" tabindex="-1" role="dialog" id="modalInfrastructure">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Editar actividad de: Infraestructura</h4>
                </div>
                <form action="{{ url('/activity/edit') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <input type="hidden" name="activity_id" id="activity_id" value="">
                        <div class="form-group">
                            <label for="activity_name">Nombre de la actividad</label>
                            <input type="text" class="form-control" name="name" id="activity_name" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Guardar cambios</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--modal edit human resources--}}
    <div class="modal fade" tabindex="-1" role="dialog" id="modalHumanResources">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Editar actividad de: Recursos humanos</h4>
                </div>
                <form action="{{ url('/activity/edit') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <input type="hidden" name="activity_id" id="resource_id" value="">
                        <div class="form-group">
                            <label for="resource_name">Nombre de la actividad</label>
                            <input type="text" class="form-control" name="name" id="resource_name" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Guardar cambios</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--modal edit development--}}
    <div class="modal fade" tabindex="-1" role="dialog" id="modalDevelopment">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Editar actividad de: Desarrollo tecnológico</h4>
                </div>
                <form action="{{ url('/activity/edit') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <input type="hidden" name="activity_id" id="development_id" value="">
                        <div class="form-group">
                            <label for="development_name">Nombre de la actividad</label>
                            <input type="text" class="form-control" name="name" id="development_name" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Guardar cambios</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--modal edit catering--}}
    <div class="modal fade" tabindex="-1" role="dialog" id="modalCatering">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Editar actividad de: Abastecimiento</h4>
                </div>
                <form action="{{ url('/activity/edit') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <input type="hidden" name="activity_id" id="catering_id" value="">
                        <div class="form-group">
                            <label for="catering_name">Nombre de la actividad</label>
                            <input type="text" class="form-control" name="name" id="catering_name" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Guardar cambios</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-----}}
    {{--modal new internal logistics--}}
    <div id="new-internal" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Registro para: Logística interna</h4>
                </div>
                <form class="form-group" role="form" action="{{ url('/activity/create') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <p class="text-muted m-b-15 font-13">
                            Se esta realizando el registro de una nueva actividad en el campo: <strong>Logística interna.</strong><br>
                            De lo contrario escoja otro campo...
                        </p>
                        <div class="form-group">
                            <label for="name" class="control-label">Nombre de la actividad</label>
                            <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}">
                            <input type="hidden" name="category" value="Logística interna">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Registrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--modal edit internal logistics--}}
    <div class="modal fade" tabindex="-1" role="dialog" id="modalInternal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Editar actividad de: Logística interna</h4>
                </div>
                <form action="{{ url('/activity/edit') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <input type="hidden" name="activity_id" id="internal_id" value="">
                        <div class="form-group">
                            <label for="internal_name">Nombre de la actividad</label>
                            <input type="text" class="form-control" name="name" id="internal_name" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Guardar cambios</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{--modal new operation--}}
    <div id="new-operation" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Registro para: Operaciones</h4>
                </div>
                <form class="form-group" role="form" action="{{ url('/activity/create') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <p class="text-muted m-b-15 font-13">
                            Se esta realizando el registro de una nueva actividad en el campo: <strong>Operaciones.</strong><br>
                            De lo contrario escoja otro campo...
                        </p>
                        <div class="form-group">
                            <label for="name" class="control-label">Nombre de la actividad</label>
                            <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}">
                            <input type="hidden" name="category" value="Operaciones">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Registrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--modal edit operation--}}
    <div class="modal fade" tabindex="-1" role="dialog" id="modalOperation">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Editar actividad de: Operaciones</h4>
                </div>
                <form action="{{ url('/activity/edit') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <input type="hidden" name="activity_id" id="operation_id" value="">
                        <div class="form-group">
                            <label for="operation_name">Nombre de la actividad</label>
                            <input type="text" class="form-control" name="name" id="operation_name" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Guardar cambios</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{--modal new service--}}
    <div id="new-service" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Registro para: Servicio post-venta</h4>
                </div>
                <form class="form-group" role="form" action="{{ url('/activity/create') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <p class="text-muted m-b-15 font-13">
                            Se esta realizando el registro de una nueva actividad en el campo: <strong>Servicio post-venta.</strong><br>
                            De lo contrario escoja otro campo...
                        </p>
                        <div class="form-group">
                            <label for="name" class="control-label">Nombre de la actividad</label>
                            <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}">
                            <input type="hidden" name="category" value="Servicio post-venta">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Registrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--modal edit service--}}
    <div class="modal fade" tabindex="-1" role="dialog" id="modalService">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Editar actividad de: Servicio post-venta</h4>
                </div>
                <form action="{{ url('/activity/edit') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <input type="hidden" name="activity_id" id="service_id" value="">
                        <div class="form-group">
                            <label for="service_name">Nombre de la actividad</label>
                            <input type="text" class="form-control" name="name" id="service_name" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Guardar cambios</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{--modal new external logistics--}}
    <div id="new-external" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Registro para: Logística externa</h4>
                </div>
                <form class="form-group" role="form" action="{{ url('/activity/create') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <p class="text-muted m-b-15 font-13">
                            Se esta realizando el registro de una nueva actividad en el campo: <strong>Logística externa.</strong><br>
                            De lo contrario escoja otro campo...
                        </p>
                        <div class="form-group">
                            <label for="name" class="control-label">Nombre de la actividad</label>
                            <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}">
                            <input type="hidden" name="category" value="Logística externa">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Registrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--modal edit external logistics--}}
    <div class="modal fade" tabindex="-1" role="dialog" id="modalExternal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Editar actividad de: Logística externa</h4>
                </div>
                <form action="{{ url('/activity/edit') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <input type="hidden" name="activity_id" id="external_id" value="">
                        <div class="form-group">
                            <label for="external_name">Nombre de la actividad</label>
                            <input type="text" class="form-control" name="name" id="external_name" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Guardar cambios</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{--modal new marketing--}}
    <div id="new-marketing" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Registro para: Mercadotecnia y ventas</h4>
                </div>
                <form class="form-group" role="form" action="{{ url('/activity/create') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <p class="text-muted m-b-15 font-13">
                            Se esta realizando el registro de una nueva actividad en el campo: <strong>Mercadotecnia y ventas.</strong><br>
                            De lo contrario escoja otro campo...
                        </p>
                        <div class="form-group">
                            <label for="name" class="control-label">Nombre de la actividad</label>
                            <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}">
                            <input type="hidden" name="category" value="Mercadotecnia y ventas">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Registrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--modal edit marketing--}}
    <div class="modal fade" tabindex="-1" role="dialog" id="modalMarketing">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Editar actividad de: Mercadotecnia y ventas</h4>
                </div>
                <form action="{{ url('/activity/edit') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <input type="hidden" name="activity_id" id="marketing_id" value="">
                        <div class="form-group">
                            <label for="marketing_name">Nombre de la actividad</label>
                            <input type="text" class="form-control" name="name" id="marketing_name" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Guardar cambios</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- Modal-Effect -->
    <script src="{{ asset('/plugins/custombox/dist/custombox.min.js') }}"></script>
    <script src="{{ asset('/plugins/custombox/dist/legacy.min.js"><') }}"></script>

    <script src="{{ asset('/js/admin/generalities/edit.js') }}"></script>
@endsection