<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!-- User -->
        <div class="user-box">
            <p class="font-600 text-primary text-center" style="padding: 0 12px">Empresa: {{ company()->name }}</p>
            <div class="user-img">
                <img src="{{ asset('/images/users/avatar-1.jpg') }}" alt="user-img" title="Mat Helme" class="img-circle img-thumbnail img-responsive">
                <div class="user-status offline"><i class="zmdi zmdi-dot-circle"></i></div>
            </div>
            <h5><a href="#">{{ auth()->user()->name_last_name }}</a></h5>
            <h5 class="font-13 text-muted">{{ auth()->user()->role_name }}</h5>
            <ul class="list-inline">
                <li>
                    <a href="#" >
                        <i class="zmdi zmdi-settings"></i>
                    </a>
                </li>

                <li>
                    <a href="{{ url('/logout') }}"
                       onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();" title="Cerrar sesión">
                        <i class="zmdi zmdi-power"></i>
                    </a>
                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </div>
        <!-- End User -->
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul>
                <li class="text-muted menu-title">Menú</li>

                <li>
                    <a href="{{ url('/home') }}" class="waves-effect">
                        <i class="zmdi zmdi-home"></i>
                        <span> Inicio </span>
                    </a>
                </li>
                @if (auth()->user()->is_admin)
                    <li>
                        <a href="{{ url('/generalities') }}" class="waves-effect">
                            <i class="fa fa-building"></i>
                            <span> Generalidades </span>
                        </a>
                    </li>
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-table"></i> <span> Cadena de valor </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('/value-chain') }}">Gestionar actividades</a></li>
                            <li><a href="{{ url('/value-chain/graphic') }}">Mostrar gráfico</a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-line-chart"></i> <span> Cadena de suministros </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('/supply-chain/provider') }}">Datos proveedor</a></li>
                            <li><a href="{{ url('/supply-chain/customer') }}">Datos cliente</a></li>
                            <li><a href="{{ url('/supply-chain/graphic') }}">Mostrar gráfico</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ url('/users') }}" class="waves-effect">
                            <i class="fa fa-users"></i>
                            <span> Gestionar usuarios </span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/companies') }}" class="waves-effect">
                            <i class="zmdi zmdi-city"></i>
                            <span> Gestionar empresas </span>
                        </a>
                    </li>
                @endif
                @if (auth()->user()->is_user)
                    <li>
                        <a href="{{ url('/generalities') }}" class="waves-effect">
                            <i class="fa fa-building"></i>
                            <span> Generalidades </span>
                        </a>
                    </li>
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-table"></i> <span> Cadena de valor </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('/value-chain/data') }}">Mostrar datos</a></li>
                            <li><a href="{{ url('/value-chain/graphic') }}">Mostrar gráfico</a></li>
                        </ul>
                    </li>
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-line-chart"></i> <span> Cadena de suministros </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('/supply-chain/data') }}">Mostrar datos</a></li>
                            <li><a href="{{ url('/supply-chain/graphic') }}">Mostrar gráfico</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
            <div class="clearfix"></div>
        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>

    </div>

</div>
<!-- Left Sidebar End -->