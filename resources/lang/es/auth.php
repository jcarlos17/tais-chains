<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Los datos ingresados no son correctos, o su acceso aún no se ha aprobado.',
    'throttle' => 'Muchos intentos de inicio de sesión fallidos. Intenta nuevamente en :seconds segundos.',

];
