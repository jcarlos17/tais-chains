<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use softDeletes;


    protected $fillable = [
        'name', 'last_name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getIsAdminAttribute()
    {
        return $this->role == 0;
    }
    public function getIsUserAttribute()
    {
        return $this->role == 1;
    }

    public function getNameLastNameAttribute()
    {
        $name = $this->name;
        $last_name = $this->last_name;
        $first_name = explode(" ",$name);
        $first_last_name = explode(" ",$last_name);

        return $first_name[0]. ' ' .$first_last_name[0];
    }

    public function getRoleNameAttribute() // accessor role_name
    {
        if ($this->role == 0)
            return 'Administrador';
        if ($this->role == 1)
            return 'Usuario estándar';
    }
}
