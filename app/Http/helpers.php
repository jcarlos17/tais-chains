<?php


use App\Company;

function company()
{
    return Company::where('id', session('company_id'))->first();
}
