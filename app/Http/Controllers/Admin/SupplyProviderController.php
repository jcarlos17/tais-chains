<?php

namespace App\Http\Controllers\Admin;

use App\Company;
use App\SupplyChainProvider;
use App\SupplyChainProviderRelation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SupplyProviderController extends Controller
{
    public function byOrigin($id)
    {
        $provider = SupplyChainProvider::find($id);
        $company_id = $provider->company_id;
        $level = $provider->level -1;
        return SupplyChainProvider::where('level', $level)->where('company_id', $company_id)->get();
    }

    public function index()
    {
        $company_id = session('company_id');
        $company = Company::find($company_id);
        $components = SupplyChainProvider::where('company_id', $company->id)->orderBy('level', 'desc')->get();
        foreach ($components as $component){
            $component_id = $component->id;
            $component->editable = SupplyChainProviderRelation::where('origin_provider_id', $component_id)->orWhere('destiny_provider_id', $component_id)->exists();
        }

        $relations = SupplyChainProviderRelation::where('company_id', $company_id)->get();
        return view('admin.supply-provider.index')->with(compact('company', 'components', 'relations'));
    }

    public function addLevel($id)
    {
        $addLevel = Company::find($id);
        $addLevel->provider_levels_count+=1;
        $addLevel->save();

        $notification = 'Se agregó un nuevo nivel';
        return back()->with(compact('notification'));
    }

    public function removeLevel($id)
    {
        $removeLevel = Company::find($id);
        $levelExists = SupplyChainProvider::where('level', $removeLevel->provider_levels_count)->where('company_id', $id)->exists();

        if (! $levelExists){
            $removeLevel->provider_levels_count-=1;
            $removeLevel->save();

            return back()->with('notification', 'Se eliminó correctamente el nivel');
        }else{
            return back()->with('error', 'No se puede eliminar el nivel '.$removeLevel->provider_levels_count.' porque existe componentes de este nivel');
        }
    }

    public function storeComponent(Request $request)
    {
        $component = new SupplyChainProvider();
        $component->name = $request->input('name');
        $component->level = $request->input('level');
        $component->company_id = session('company_id');
        $component->user_id = auth()->id();
        $component->save();

        $notification = 'Se registró correctamente el nuevo componente';
        return back()->with(compact('notification'));
    }

    public function updateComponent(Request $request)
    {
        $id = $request->input('component_id');

        $component = SupplyChainProvider::find($id);

        $component->name = $request->input('name');
        $level = $request->input('level');
        if($level)
            $component->level = $level;
        $component->save();

        return back()->with('notification', 'El componente fue modificado correctamente');
    }

    public function deleteComponent($id)
    {
        $componentExists = SupplyChainProviderRelation::where('origin_provider_id', $id)->orWhere('destiny_provider_id', $id)->exists();
        $component = SupplyChainProvider::find($id);
        if (! $componentExists){

            $component->delete();

            return back()->with('notification', 'Se eliminó correctamente el componente');
        }else{
            return back()->with('error', 'No se puede eliminar el componente '.$component->name.' porque existe en las relaciones');
        }

    }

    public function storeRelation(Request $request)
    {
        $origin_id = $request->input('origin_id');
        $destiny_id = $request->input('destiny_id');
        $componentExists = SupplyChainProviderRelation::where('origin_provider_id', $origin_id)
            ->where('destiny_provider_id', $destiny_id)->exists();

        if (!$componentExists) {

            $component = new SupplyChainProviderRelation();
            $component->origin_provider_id = $origin_id;
            $component->destiny_provider_id = $destiny_id;
            $component->company_id = session('company_id');
            $component->user_id = auth()->id();
            $component->save();

            return back()->with('notification', 'Se registró exitosamente la relación');
        }else{
            return back()->with('error', 'La relación ya está registrada');
        }
    }

    public function deleteRelation($relation_id)
    {
        $relation = SupplyChainProviderRelation::find($relation_id);
        $relation->delete();
        return back()->with('notification', 'Se eliminó exitosamente la relación');
    }
}
