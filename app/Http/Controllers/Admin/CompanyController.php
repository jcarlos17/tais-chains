<?php

namespace App\Http\Controllers\Admin;

use App\Company;
use App\SupplyChainCustomer;
use App\SupplyChainProvider;
use App\ValueChainActivity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompanyController extends Controller
{
    public function index()
    {
        $companies = Company::all();

        return view('admin.companies.index')->with(compact('companies'));
    }

    public function delete($id)
    {
        $company = Company::find($id);
        $company_id = $company->id;
        $valueExists = ValueChainActivity::where('company_id', $company_id)->exists();
        $supplyProviderExists = SupplyChainProvider::where('company_id', $company_id)->exists();
        $supplyCustomerExists = SupplyChainCustomer::where('company_id', $company_id)->exists();

        if($valueExists or $supplyCustomerExists or $supplyProviderExists){
            return back()->with('error', 'No se puede eliminar la empresa '.$company->name.' porque existe datos en la cadena de valor o suministros');
        }
        else{
            $company->delete();
            return back()->with('notification', 'Se eliminó correctamente');
        }
    }
}
