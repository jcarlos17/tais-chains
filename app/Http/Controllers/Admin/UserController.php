<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index()
    {
        $users = User::where('role', 1)->get();
        return view('admin.users.index')->with(compact('users'));
    }

    public function activate($id)
    {
        $user = User::find($id);
        $user->active = 1;
        $user->save();

        return back()->with('notification','Usuario '.$user->name.' fue activado correctamente');
    }

    public function deactivate($id)
    {
        $user = User::find($id);
        $user->active = 0;
        $user->save();

        return back()->with('notification','Usuario '.$user->name.' fue desactivado correctamente');
    }

    public function edit($id)
    {
        $user = User::find($id);

        return view('admin.users.edit')->with(compact('user'));
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|max:256',
            'last_name' => 'required|max:256',
            'email' => "required|email|max:255|unique:users,email,$id",
            'password' => 'nullable|min:6'
        ];
        $messages = [
            'name.required' => 'Es indispensable ingresar su nombre.',
            'name.max' => 'Su nombre es demasiado extenso.',
            'last_name.required' => 'Es indispensable ingresar sus apellidos.',
            'last_name.max' => 'Su apellido es demasiado extenso.',
            'email.required' => 'Es indispensable ingresar su correo.',
            'email.email' => 'El e-mail ingresado no es válido.',
            'email.max' => 'El e-mail es demasiado extenso.',
            'email.unique' => 'El e-mail ya se encuentra en uso.',
            'password.min' => 'La contraseña debe presentar al menos 6 caracteres.',
        ];
        $this->validate($request, $rules, $messages);

        $user = User::find($id);
        $user->name = $request->input('name');
        $user->last_name = $request->input('last_name');
        $user->email = $request->input('email');
        $password = $request->input('password');
        if ($password)
            $user->password = bcrypt($password);

        $user->save();

        return redirect('/users')->with('notification', 'Usuario modificado exitosamente.');
    }

    public function delete($id)
    {
        $user = User::find($id);
        $user->delete();
        return back()->with('notification','Se eliminó el usuario correctamente');
    }
}
