<?php

namespace App\Http\Controllers\Admin;

use App\ValueChainActivity;
use App\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ValueController extends Controller
{
    public function index()
    {
        $company_id = session('company_id');
        $company = Company::find($company_id);
        //Actividades de apoyo
        $infrastructures = ValueChainActivity::where('company_id', $company_id)->where('category','Infraestructura')->get();
        $resources = ValueChainActivity::where('company_id', $company_id)->where('category','Recursos humanos')->get();
        $developments = ValueChainActivity::where('company_id', $company_id)->where('category','Desarrollo tecnológico')->get();
        $caterings = ValueChainActivity::where('company_id', $company_id)->where('category','Abastecimiento')->get();

        //Actividades principales
        $internals = ValueChainActivity::where('company_id', $company_id)->where('category','Logística interna')->get();
        $operations = ValueChainActivity::where('company_id', $company_id)->where('category','Operaciones')->get();
        $services = ValueChainActivity::where('company_id', $company_id)->where('category','Servicio post-venta')->get();
        $externals = ValueChainActivity::where('company_id', $company_id)->where('category','Logística externa')->get();
        $marketings = ValueChainActivity::where('company_id', $company_id)->where('category','Mercadotecnia y ventas')->get();

        return view('admin.value.index')
            ->with(compact(
                'infrastructures', 'resources', 'developments', 'caterings',
                'internals', 'operations', 'services', 'externals', 'marketings', 'company'
            ));
    }

    public function store(Request $request)
    {
        $activity = new ValueChainActivity();
        $activity->category = $request->input('category');
        $activity->name = $request->input('name');
        $activity->company_id = session('company_id');
        $activity->user_id = auth()->id();
        $activity->save();

        return back()->with('notification', 'Se registro correctamente');

    }

    public function update(Request $request)
    {
        $activity_id = $request->input('activity_id');

        $activity = ValueChainActivity::find($activity_id);
        $activity->name = $request->input('name');
        $activity->save();

        return back()->with('notification', 'Se modificó correctamente');

    }

    public function delete($id)
    {
        $activity = ValueChainActivity::find($id);
        $activity->delete();

        return back()->with('notification', 'Se eliminó correctamente');
    }

}
