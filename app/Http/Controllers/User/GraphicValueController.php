<?php

namespace App\Http\Controllers\User;

use App\Company;
use App\ValueChainActivity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GraphicValueController extends Controller
{

    public function index()
    {
        $company = Company::find(session('company_id'));
        //Actividades de apoyo
        $infrastructures = ValueChainActivity::where('company_id', session('company_id'))->where('category','Infraestructura')->get();
        $resources = ValueChainActivity::where('company_id', session('company_id'))->where('category','Recursos humanos')->get();
        $developments = ValueChainActivity::where('company_id', session('company_id'))->where('category','Desarrollo tecnológico')->get();
        $caterings = ValueChainActivity::where('company_id', session('company_id'))->where('category','Abastecimiento')->get();

        //Actividades principales
        $internals = ValueChainActivity::where('company_id', session('company_id'))->where('category','Logística interna')->get();
        $operations = ValueChainActivity::where('company_id', session('company_id'))->where('category','Operaciones')->get();
        $services = ValueChainActivity::where('company_id', session('company_id'))->where('category','Servicio post-venta')->get();
        $externals = ValueChainActivity::where('company_id', session('company_id'))->where('category','Logística externa')->get();
        $marketings = ValueChainActivity::where('company_id', session('company_id'))->where('category','Mercadotecnia y ventas')->get();

        return view('value-chain.index')
            ->with(compact(
                'infrastructures', 'resources', 'developments', 'caterings',
                'internals', 'operations', 'services', 'externals', 'marketings', 'company'
            ));
    }

    public function show()
    {
        //Actividades de apoyo
        $infrastructures = ValueChainActivity::where('company_id', session('company_id'))->where('category','Infraestructura')->get();
        $resources = ValueChainActivity::where('company_id', session('company_id'))->where('category','Recursos humanos')->get();
        $developments = ValueChainActivity::where('company_id', session('company_id'))->where('category','Desarrollo tecnológico')->get();
        $caterings = ValueChainActivity::where('company_id', session('company_id'))->where('category','Abastecimiento')->get();

        $infrastructureN = $infrastructures->count();
        $resourceN = $resources->count();
        $developmentN = $developments->count();
        $cateringN = $caterings->count();

        $sum_AP = $infrastructureN + $resourceN + $developmentN + $cateringN;

        //Actividades principales
        $internals = ValueChainActivity::where('company_id', session('company_id'))->where('category','Logística interna')->get();
        $operations = ValueChainActivity::where('company_id', session('company_id'))->where('category','Operaciones')->get();
        $services = ValueChainActivity::where('company_id', session('company_id'))->where('category','Servicio post-venta')->get();
        $externals = ValueChainActivity::where('company_id', session('company_id'))->where('category','Logística externa')->get();
        $marketings = ValueChainActivity::where('company_id', session('company_id'))->where('category','Mercadotecnia y ventas')->get();

        $internalN = $internals->count();
        $operationN = $operations->count();
        $serviceN = $services->count();
        $externalN = $externals->count();
        $marketingN = $marketings->count();

        $maxN = max([$internalN, $operationN, $serviceN, $externalN, $marketingN]);



        return view('value-chain.show')
            ->with(compact(
                'infrastructures', 'resources', 'developments', 'caterings',
                'internals', 'operations', 'services', 'externals', 'marketings', 'maxN', 'sum_AP'
            ));
    }
}
