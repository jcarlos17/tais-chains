<?php

namespace App\Http\Controllers\User;

use App\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GeneralityController extends Controller
{
    public function show()
    {
        $company = Company::find(session('company_id'));
        return view('generality.show', compact('company'));
    }
}
