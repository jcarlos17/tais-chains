<?php

namespace App\Http\Controllers\User;

use App\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{
    public function getSelect()
    {
        $companies = Company::all();
        return view('company.select', compact('companies'));
    }

    public function postSelect(Request $request)
    {
        session()->put('company_id', $request->company_id);
        return redirect('home');
    }

    public function create()
    {
        return view('company.create');
    }

    public function store(Request $request)
    {

        $rules = [
            'name' => 'required|max:255',
            'ruc' => 'required|min:11|max:11|unique:companies',
            'type_taxpayer' => 'required',
            'business_turn' => 'required',
            'tax_residence' => 'required',
        ];
        $messages = [
            'name.required' => 'Es necesario ingresar el nombre empresarial',
            'name.max' => 'El nombre empresarial es demasiado extenso.',
            'ruc.required' => 'Es necesario ingresar el ruc',
            'ruc.min' => 'El ruc debe tener 11 dígitos',
            'ruc.max' => 'El ruc debe tener 11 dígitos',
            'ruc.unique' => 'El e-mail ya se encuentra en uso.',
            'type_taxpayer.required' => 'Es necesario ingresar el nombre empresarial',
            'business_turn.required' => 'Es necesario ingresar giro del negocio',
            'tax_residence.required' => 'Es necesario ingresar domicilio fiscal',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        $validator->after(function ($validator) use ($request) {
            $distinct_number = count(array_unique(str_split($request->input('ruc'))));

            if ($distinct_number < 4) {
                $validator->errors()->add('ruc', 'Ingrese un ruc válido');
            }
        });
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $company = new Company();
        $company->name = $request->input('name');
        $company->ruc = $request->input('ruc');
        $company->type_taxpayer = $request->input('type_taxpayer');
        $company->business_turn = $request->input('business_turn');
        $company->tax_residence = $request->input('tax_residence');
        $company->phone = $request->input('phone');
        $company->email = $request->input('email');
        $company->location = $request->input('location');
        $company->mission = $request->input('mission');
        $company->vision = $request->input('vision');
        $company->value_proposal = $request->input('value_proposal');
        $company->factor = $request->input('factor');
        $company->business_role = $request->input('business_role');
        $company->user_id = auth()->id();

        $company->save();

        return redirect('/company/select')->with('notification', 'La empresa fue registrado correctamente.');
    }
}
