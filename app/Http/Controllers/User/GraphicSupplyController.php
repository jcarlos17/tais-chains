<?php

namespace App\Http\Controllers\User;

use App\SupplyChainCustomer;
use App\SupplyChainCustomerRelation;
use App\SupplyChainProvider;
use App\SupplyChainProviderRelation;
use App\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GraphicSupplyController extends Controller
{
    public function index()
    {
        $company_id = session('company_id');

        $company = Company::find($company_id);

        $c_components = SupplyChainCustomer::where('company_id', $company->id)->get();
        $c_relations = SupplyChainCustomerRelation::where('company_id', $company_id)->get();

        $p_components = SupplyChainProvider::where('company_id', $company->id)->orderBy('level', 'desc')->get();
        $p_relations = SupplyChainProviderRelation::where('company_id', $company_id)->get();

        return view('supply-chain.index')
            ->with(compact(
                'company','c_components', 'c_relations',
                'p_components', 'p_relations'));
    }

    public function show()
    {
        $company_id = session('company_id');

        $company = Company::find($company_id);

        $customers = SupplyChainCustomer::where('company_id', $company_id)->get();
        $providers = SupplyChainProvider::where('company_id', $company_id)->get();

        $Lv1_customers = SupplyChainCustomer::where('company_id', $company_id)->where('level',1)->get();
        $Lv1_providers = SupplyChainProvider::where('company_id', $company_id)->where('level',1)->get();

        $customerRels = SupplyChainCustomerRelation::where('company_id', $company_id)->get();
        $providerRels = SupplyChainProviderRelation::where('company_id', $company_id)->get();

        return view('supply-chain.show')
            ->with(compact(
                'company','customers', 'providers',
                'Lv1_customers', 'Lv1_providers',
                'customerRels', 'providerRels'));
    }

    public function updatePosition(Request $request)
    {
        $graphId = $request->input('id');
        $x = $request->input('x');
        $y = $request->input('y');

        if (substr($graphId, 0, 8) === "provider") {
            $id = substr($graphId, 8);
            // dd($id);
            $element = SupplyChainProvider::find($id);
        } elseif (substr($graphId, 0, 8) === "customer") {
            $id = substr($graphId, 8);
            $element = SupplyChainCustomer::find($id);
        } else { // company
            $id = substr($graphId, 7);
            $element = Company::find($id);
        }

        $element->x = $x;
        $element->y = $y;
        $saved = $element->save();

        $data['success'] = $saved;
        return $data;
    }
}
