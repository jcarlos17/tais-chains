<?php namespace App\Http\Controllers\User;

use App\Company;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        $company = Company::find(session('company_id'));
        return view('home')->with(compact('company'));
    }
}
