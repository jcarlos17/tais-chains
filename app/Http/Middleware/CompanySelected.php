<?php

namespace App\Http\Middleware;

use App\Company;
use Closure;

class CompanySelected
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session('company_id') and Company::find(session('company_id')))
            return $next($request);

        return redirect('/company/select');
    }
}
