<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplyChainProviderRelation extends Model
{
    public function origin()
    {
        return $this->belongsTo(SupplyChainProvider::class, 'origin_provider_id');
    }

    public function destiny()
    {
        return $this->belongsTo(SupplyChainProvider::class, 'destiny_provider_id');
    }

    public function getNameRelationAttribute()
    {
        return $this->origin->name.' ('.$this->origin->level.')'.' - '.$this->destiny->name.' ('.$this->destiny->level.')';
    }
}
