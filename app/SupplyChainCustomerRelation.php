<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplyChainCustomerRelation extends Model
{
    public function origin()
    {
        return $this->belongsTo(SupplyChainCustomer::class, 'origin_customer_id');
    }

    public function destiny()
    {
        return $this->belongsTo(SupplyChainCustomer::class, 'destiny_customer_id');
    }

    public function getNameRelationAttribute()
    {
        return $this->origin->name.' ('.$this->origin->level.')'.' - '.$this->destiny->name.' ('.$this->destiny->level.')';
    }

}
