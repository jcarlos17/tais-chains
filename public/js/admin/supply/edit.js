$(function() {

    $('[data-component]').on('click', editComponentModal);
    //select level
    $('#select-origin').on('change', onSelectOriginChange);
    //select level Provider
    $('#select-origin-provider').on('change', onSelectOriginProviderChange);

});

function editComponentModal() {
    // id
    var component_id = $(this).data('component');
    $('#component_id').val(component_id);
    //editable
    var editable = $(this).data('editable');
    //name
    var component_name = $(this).parent().prev().prev().text();
    $('#component_name').val(component_name);
    // level
    var component_level = $(this).parent().prev().text();
    if(editable)
        $('#component_level').val(component_level).prop('disabled', true);
    else
        $('#component_level').val(component_level).prop('disabled', false);
    // show
    $('#modalComponent').modal('show');
}

function onSelectOriginChange() {
    var origin_id = $(this).val();

    if (! origin_id) {
        $('#select-destiny').html('<option value="">Seleccione destino</option>');
        return;
    }

    // AJAX
    $.get('/api/origin/'+origin_id+'/destinations', function (data) {
        var html_select = '<option value="">Seleccione destino</option>';
        for (var i=0; i<data.length; ++i)
            html_select += '<option value="'+data[i].id+'">'+data[i].name+' ('+data[i].level+')</option>';
        $('#select-destiny').html(html_select);
    });
}

function onSelectOriginProviderChange() {
    var origin_id = $(this).val();

    if (! origin_id) {
        $('#select-destiny-provider').html('<option value="">Seleccione destino</option>');
        return;
    }

    // AJAX
    $.get('/api/origin-provider/'+origin_id+'/destinations', function (data) {
        var html_select = '<option value="">Seleccione destino</option>';
        for (var i=0; i<data.length; ++i)
            html_select += '<option value="'+data[i].id+'">'+data[i].name+' ('+data[i].level+')</option>';
        $('#select-destiny-provider').html(html_select);
    });
}