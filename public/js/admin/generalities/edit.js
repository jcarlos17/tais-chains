$(function() {
    //
    $('[data-infrastructure]').on('click', editInfrastructureModal);
    $('[data-resource]').on('click', editResourcesModal);
    $('[data-development]').on('click', editDevelopmentModal);
    $('[data-catering]').on('click', editCateringModal);

    //
    $('[data-internal]').on('click', editInternalModal);
    $('[data-operation]').on('click', editOperationModal);
    $('[data-service]').on('click', editServiceModal);
    $('[data-external]').on('click', editExternalModal);
    $('[data-marketing]').on('click', editMarketingModal);
});

function editInfrastructureModal() {
    // id
    var activity_id = $(this).data('infrastructure');
    $('#activity_id').val(activity_id);
    // name
    var activity_name = $(this).parent().prev().text();
    $('#activity_name').val(activity_name);
    // show
    $('#modalInfrastructure').modal('show');
}

function editResourcesModal() {
    // id
    var resource_id = $(this).data('resource');
    $('#resource_id').val(resource_id);
    // name
    var resource_name = $(this).parent().prev().text();
    $('#resource_name').val(resource_name);
    // show
    $('#modalHumanResources').modal('show');
}

function editDevelopmentModal() {
    // id
    var development_id = $(this).data('development');
    $('#development_id').val(development_id);
    // name
    var development_name = $(this).parent().prev().text();
    $('#development_name').val(development_name);
    // show
    $('#modalDevelopment').modal('show');
}

function editCateringModal() {
    // id
    var catering_id = $(this).data('catering');
    $('#catering_id').val(catering_id);
    // name
    var catering_name = $(this).parent().prev().text();
    $('#catering_name').val(catering_name);
    // show
    $('#modalCatering').modal('show');
}

//
function editInternalModal() {
    // id
    var internal_id = $(this).data('internal');
    $('#internal_id').val(internal_id);
    // name
    var internal_name = $(this).parent().prev().text();
    $('#internal_name').val(internal_name);
    // show
    $('#modalInternal').modal('show');
}

function editOperationModal() {
    // id
    var operation_id = $(this).data('operation');
    $('#operation_id').val(operation_id);
    // name
    var operation_name = $(this).parent().prev().text();
    $('#operation_name').val(operation_name);
    // show
    $('#modalOperation').modal('show');
}

function editServiceModal() {
    // id
    var service_id = $(this).data('service');
    $('#service_id').val(service_id);
    // name
    var service_name = $(this).parent().prev().text();
    $('#service_name').val(service_name);
    // show
    $('#modalService').modal('show');
}

function editExternalModal() {
    // id
    var external_id = $(this).data('external');
    $('#external_id').val(external_id);
    // name
    var external_name = $(this).parent().prev().text();
    $('#external_name').val(external_name);
    // show
    $('#modalExternal').modal('show');
}

function editMarketingModal() {
    // id
    var marketing_id = $(this).data('marketing');
    $('#marketing_id').val(marketing_id);
    // name
    var marketing_name = $(this).parent().prev().text();
    $('#marketing_name').val(marketing_name);
    // show
    $('#modalMarketing').modal('show');
}