<?php

use App\Company;
use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Company::create([
            'name' => 'Gica',
            'user_id' => 1
        ]);

        Company::create([
            'name' => 'Sedalib',
            'user_id' => 1
        ]);
    }
}
