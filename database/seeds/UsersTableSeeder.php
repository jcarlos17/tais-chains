<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    public function run()
    {
        User::create([
            'name' => 'Pedro Cristhian',
            'last_name' => 'Ramirez Salcedo',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123123'),
            'role' => 0,
            'active' => true
        ]);
    }
}
