<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('ruc')->unique()->nullable();
            $table->string('type_taxpayer')->nullable();
            $table->string('business_turn')->nullable();
            $table->string('tax_residence')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('location')->nullable();
            $table->text('mission')->nullable();
            $table->text('vision')->nullable();
            $table->text('value_proposal')->nullable();
            $table->text('factor')->nullable();
            $table->text('business_role')->nullable();

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            // levels count for supply chain +=1
            $table->unsignedSmallInteger('provider_levels_count')->default(0);
            $table->unsignedSmallInteger('customer_levels_count')->default(0);

            // graph position for this node
            $table->unsignedSmallInteger('x')->default(430);
            $table->unsignedSmallInteger('y')->default(150);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
