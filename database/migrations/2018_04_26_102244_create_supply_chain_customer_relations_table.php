<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplyChainCustomerRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supply_chain_customer_relations', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('origin_customer_id')->unsigned();
            $table->foreign('origin_customer_id')->references('id')->on('supply_chain_customers');

            $table->integer('destiny_customer_id')->unsigned();
            $table->foreign('destiny_customer_id')->references('id')->on('supply_chain_customers');

            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supply_chain_customer_relations');
    }
}
