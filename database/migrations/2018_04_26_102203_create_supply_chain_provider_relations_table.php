<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplyChainProviderRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supply_chain_provider_relations', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('origin_provider_id')->unsigned();
            $table->foreign('origin_provider_id')->references('id')->on('supply_chain_providers');

            $table->integer('destiny_provider_id')->unsigned();
            $table->foreign('destiny_provider_id')->references('id')->on('supply_chain_providers');

            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supply_chain_provider_relations');
    }
}
