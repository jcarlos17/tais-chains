<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth', 'namespace' => 'User'], function (){
    // Select company
    Route::get('/company/select', 'CompanyController@getSelect');
    Route::post('/company/select', 'CompanyController@postSelect');

    Route::get('/company/register', 'CompanyController@create');
    Route::post('/company/register', 'CompanyController@store');
});

Route::group(['middleware' => ['auth', 'company_selected'], 'namespace' => 'User'], function (){

    Route::get('/home', 'HomeController@index')->name('home');

    // Generalities company
    Route::get('/generalities', 'GeneralityController@show');

    //value chain
    Route::get('/value-chain/data', 'GraphicValueController@index');
    Route::get('/value-chain/graphic', 'GraphicValueController@show');

    //value chain
    Route::get('/supply-chain/data', 'GraphicSupplyController@index');
    Route::get('/supply-chain/graphic', 'GraphicSupplyController@show');

    // Graph events
    Route::get('/supply-chain/graphic/position', 'GraphicSupplyController@updatePosition');
});

Route::group(['middleware' => ['auth','company_selected', 'admin'], 'namespace' => 'Admin'], function (){

    //generalities company edit
    Route::get('/generalities/{id}/edit', 'GeneralityController@edit');
    Route::post('/generalities/{id}/edit', 'GeneralityController@update');

    //value chain
    Route::get('/value-chain', 'ValueController@index');
    Route::post('/activity/create', 'ValueController@store');
    Route::post('/activity/edit', 'ValueController@update');
    Route::get('/activity/{id}/delete', 'ValueController@delete');

    // Supply Chain
    // Provider
    Route::get('/supply-chain/provider', 'SupplyProviderController@index');
    Route::get('/supply-chain/provider/{company_id}/addLevel', 'SupplyProviderController@addLevel');
    Route::get('/supply-chain/provider/{company_id}/removeLevel', 'SupplyProviderController@removeLevel');
    Route::post('/supply-chain/provider/newComponent', 'SupplyProviderController@storeComponent');
    Route::post('/supply-chain/provider/changeComponent', 'SupplyProviderController@updateComponent');
    Route::get('/supply-chain/provider/{component_id}/deleteComponent', 'SupplyProviderController@deleteComponent');
    Route::post('/supply-chain/provider/newRelation', 'SupplyProviderController@storeRelation');
    Route::get('/supply-chain/provider/{relation_id}/deleteRelation', 'SupplyProviderController@deleteRelation');

    // Customer
    Route::get('/supply-chain/customer', 'SupplyCustomerController@index');
    Route::get('/supply-chain/customer/{company_id}/addLevel', 'SupplyCustomerController@addLevel');
    Route::get('/supply-chain/customer/{company_id}/removeLevel', 'SupplyCustomerController@removeLevel');
    Route::post('/supply-chain/customer/newComponent', 'SupplyCustomerController@storeComponent');
    Route::post('/supply-chain/customer/changeComponent', 'SupplyCustomerController@updateComponent');
    Route::get('/supply-chain/customer/{component_id}/deleteComponent', 'SupplyCustomerController@deleteComponent');
    Route::post('/supply-chain/customer/newRelation', 'SupplyCustomerController@storeRelation');
    Route::get('/supply-chain/customer/{relation_id}/deleteRelation', 'SupplyCustomerController@deleteRelation');

    // Users
    Route::get('/users', 'UserController@index');
    Route::get('/users/{id}/activate', 'UserController@activate');
    Route::get('/users/{id}/deactivate', 'UserController@deactivate');
    Route::get('/users/{id}/edit', 'UserController@edit');
    Route::post('/users/{id}/edit', 'UserController@update');
    Route::get('/users/{id}/delete', 'UserController@delete');

    //Companies
    Route::get('/companies', 'CompanyController@index');
    Route::get('/companies/{id}/delete', 'CompanyController@delete');
});
